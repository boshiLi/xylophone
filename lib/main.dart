import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              NoteButton(
                color: Colors.red,
                noteNumber: 1,
              ),
              NoteButton(
                color: Colors.orange,
                noteNumber: 2,
              ),
              NoteButton(
                color: Colors.yellow,
                noteNumber: 3,
              ),
              NoteButton(
                color: Colors.green,
                noteNumber: 4,
              ),
              NoteButton(
                color: Colors.blue,
                noteNumber: 5,
              ),
              NoteButton(
                color: Colors.teal,
                noteNumber: 6,
              ),
              NoteButton(
                color: Colors.purple,
                noteNumber: 7,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class NoteButton extends StatelessWidget {
  NoteButton({this.noteNumber, this.color});

  final int noteNumber;
  final Color color;
  final AudioCache player = AudioCache();

  void playWaveFile({String name, String fileType}) {
    player.play('$name.wav');
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Expanded(
      child: FlatButton(
        color: this.color,
        child: Text(
          'Play Note$noteNumber',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 15,
          ),
        ),
        onPressed: () {
          this.playWaveFile(fileType: 'wav', name: 'note$noteNumber');
        },
      ),
    );
  }
}
